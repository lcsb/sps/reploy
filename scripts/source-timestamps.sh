#!/bin/bash

#
# Copyright (C) 2023 University of Luxembourg
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy
# of the License from the LICENSE file in this repository, or at:
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

# change this with environment
DEFAULT_NOT_SOURCE_REGEX="/\\/assets\\//"
NOT_SOURCE_REGEX="${NOT_SOURCE_REGEX:-$DEFAULT_NOT_SOURCE_REGEX}"
TIMESTAMP_SUFFIX="${SUFFIX:-.timestamp}"
LOGFILE="${LOGFILE:-/dev/null}"

[ -z "$1" ] && echo "$0: no inputs specified?" >/dev/stderr

while [ -n "$1" ]
do
	echo "sourcing directory '$1' ..." >> "$LOGFILE"
	find "$1" -type f -name '*.md' | grep -v "$NOT_SOURCE_REGEX" | while read file ; do
		fn=`basename "$file"`
		dir=`dirname "$file"`
		outfn="$fn.metadata.yml"
		(
			echo "adding timestamp in '$dir' for file '$fn' ..." >> "$LOGFILE"
			cd "$dir"
			if grep -q -s "^timestamp:" "$outfn"
			then echo "... but it already exists; skipping!" >> "$LOGFILE"
			else git log -n 1 --pretty=format:$'timestamp: %cs\n' -- "$fn" >> "$outfn"
			fi
		)
	done
	shift
done
