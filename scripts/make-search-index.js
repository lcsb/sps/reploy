
/*
 * Copyright (C) 2023 University of Luxembourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License from the LICENSE file in this repository, or at:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

/*
 * make-search-index.js
 *
 * This converts a "search data" file produced by the haskell site builder into
 * a lunr.js index and saves it in JSON. Metadata for rendering search output
 * (currently titles and tags) are stored separately in an extra file.
 *
 * Installing dependencies:
 * yarnpkg add lunr
 *
 * Usage:
 * reploy ....some args.... --search-data-output tmp/search-raw.json
 * node scripts/make-search-index.js tmp/search-raw.json _site/search-index.json _site/search-metadata.json
 */

lunr = require("lunr")
fs = require("fs")

if(process.argv.length !== 5) {
  console.error('Needs exactly 3 arguments (input json, output index, output metadata).');
  process.exit(1);
}

documents = JSON.parse(fs.readFileSync(process.argv[2], {encoding: 'utf8'}))

lunr.tokenizer.separator = /(\p{P}|\p{S}|\p{Z}|\p{C})+/u

var idx = lunr(function () {
  this.ref('link')
  this.field('name', {boost: 50})
  this.field('tag', {boost: 10})
  this.field('text')

  documents.forEach(function (doc) {
    const {link, name, text, tags} = doc;
    this.add({link, name, text, tag:tags.map(x => x.join(" ")).join("\n")})
  }, this)
})

fs.writeFileSync(process.argv[3], JSON.stringify(idx), {encoding: 'utf8'})
fs.writeFileSync(process.argv[4], JSON.stringify(
    Object.fromEntries(documents.map(x => [x.link, {"name": x.name, "tags": x.tags}]))
  ), {encoding: 'utf8'})
