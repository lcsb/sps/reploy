{-
 - Copyright (C) 2023 University of Luxembourg
 -
 - Licensed under the Apache License, Version 2.0 (the "License"); you may not
 - use this file except in compliance with the License. You may obtain a copy
 - of the License from the LICENSE file in this repository, or at:
 -
 - http://www.apache.org/licenses/LICENSE-2.0
 -
 - Unless required by applicable law or agreed to in writing, software
 - distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 - WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 - License for the specific language governing permissions and limitations
 - under the License.
 -}
module FormatOpts where

import Text.Pandoc.Extensions
import Text.Pandoc.Highlighting (pygments)
import Text.Pandoc.Options

-- | Default markdown reading options for Pandoc.
markdownReadOpts =
  def
    { readerExtensions =
        Text.Pandoc.Extensions.extensionsFromList
          $ Text.Pandoc.Extensions.extensionsToList
              Text.Pandoc.Extensions.pandocExtensions
              ++ [ Text.Pandoc.Extensions.Ext_smart
                 , Ext_lists_without_preceding_blankline
                 ]
    }

-- | Default HTML writing options for Pandoc.
htmlWriteOpts :: WriterOptions
htmlWriteOpts =
  def
    { writerExtensions = enableExtension Ext_smart pandocExtensions
    , writerHighlightStyle = Just pygments
    , writerWrapText = WrapPreserve
    }

-- | Default plaintext writing options for Pandoc.
plainWriteOpts :: WriterOptions
plainWriteOpts = def {writerWrapText = WrapNone}

-- | Default options for making tables of contents with certain depth.
tocWriteOpts :: Int -> WriterOptions
tocWriteOpts n = def {writerTOCDepth = n}
