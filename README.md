
# reploy

A complete REdo of dePLOYment of the R3 sites.

This is basically a small heap of haskell that produces a static HTML site out
of a pile of markdown pages (with metadata in the YAML header blocks, similarly
to what Jekyll does), complemented with a template and assets. The template
language here is a slightly updated
[Mustache](https://en.wikipedia.org/wiki/Mustache_%28template_system%29).

## How to use this

- install the haskell platform (e.g. with https://www.haskell.org/ghcup/)
- `cabal run reploy` builds a really tiny demo site (run in project folder)
- `cabal install` produces and installs an executable `reploy` into the cabal
  path (usually `~/.cabal/bin`) -- you can use it as a program directly

Optionally, there is a docker built directly from this repository.

### Containerized use

Locally, Docker has insurmountable issues with producing the files with the
right permissions. Use `podman` instead, roughly like this:

```
podman run -ti --rm -v $PWD:/data gitlab.lcsb.uni.lu:4567/lcsb/sps/reploy -d /data
```

## Configuration and options

Run `cabal run reploy -- --help` (or just `reploy --help` if you installed the
executable) to get a complete listing of available options with the
documentation.

Generally, you will mostly need to set only `-s` (marks which directories to
source), `-d` (marks where to put the generated HTML) and `-u` (sets the URL
base in case your website won't sit in the domain root).

### Search support

Reploy can generate a plaintext JSON dump of the site that is usable with
Javascript search engines, such as [lunr](https://lunrjs.com/).

The way to produce the search indexes for lunr is described in the
documentation at the top of `scripts/make-search-index.js`. By default, this
works with the minimal search implementation present in the default template
(present in `assets/`, `templates/` and `pages/search.md`)

### Markdown pages

As the most important difference from many other site generators, there is
**no** information implicitly leaking from the directory structure of the page
sources into the structure of the site, and the minor cases where this happens
are non-default or have to be explicitly defined.

All pages should be stored in some of the (possibly multiple) source
directories specified by the `-s` option. Directory layout does not matter, all
markdown files are sourced (unless exempted by another options). Generally, one
markdown file produces one "main" resulting page at the "mount" location, and
optionally several redirect pages and (additions to) category pages.

All markdown files have to contain a YAML header that describes where the page
should go and adds a few other formatting options. The whole content of the
YAML header (together with some other data) is also made accessible to the
Mustache templates -- that way you can smuggle custom contents to the HTML
rendering machinery.

#### YAML header format

##### Required options

- `mount` (string): what should be the canonical URL of the page
- `name` (string): the name of the page for display in templates and page links
  (technically, the `name` is not required UNLESS you refer to it from the
  templates, but the templates usually do that)

##### Optional

- `template` (string): name of the template file used to render the page.
  Defaulted from command options.
- `search` (boolean, default `true`): if not set, the page is omitted from the
  search index.
- `toc` (boolean or int, default `3`): if false, no ToC is generated for the
  page. Otherwise the integer sets the depth of the ToC.
- `order` (integer or string, defaults to `name` and then `mount`): order of
  the page in page listings. Negative numbers and zero sort before strings,
  positive numbers sort after strings.
- `tags` (array of strings): list of `/`-separated hierarchical tags
  ("categories") that are assigned to the page. The page will be listed in the
  category listings accordingly.
- `redirect` (array of strings): list of mounts that should redirect to this
  page (useful e.g. for old URLs, etc).

#### Example page

```md
---
mount: /about-something
name: About something
order: -1
toc: 2
template: special.html
tags:
  - stuff/special
---

# A page about something!

Lorem ipsum etc., as usual.
```

#### Example `tag-metadata.yml`

```yaml
"":
  name: "Root tag"
  extra_message_can_be_processed_by_template: "xxxx"
test/attempts:
  name: "Testing"
  order: -1
```

`name` and `order` work just as with pages.

### Template syntax

Reploy uses the "simple" vanilla
[Mustache](http://mustache.github.io/mustache.5.html) enhanced by a single
extra construction: `{{?something}}xxx{{/something}}` will render `xxx` only if
`something` evaluates to "not falsey" (non-null, nonempty). This provides an
easy but precise way to switch 2 branches of the code given the availability of
some information in the base data, without any dependence on javascript
implementation of negated truthiness -- the switch is perfectly complementary
to `{{^something}}xxx{{/something}}` which only renders `xxx` if `something`
js-evaluates to falsey (null, empty) value.

Other constructions:
- `{{#something}}...{{/something}}` can be used to "enter" the scope of
  something or unpack an array
- `{{> template.html}}` sources another template in the place
- `{{{something}}}` renders something WITHOUT escaping the HTML special
  characters (thus allowing injection of HTML code directly)

### Link processing

All links in the document (that is, markdown links `[title](url)` and `<img>`
sources) are processed by function `processLink` in `reploy.hs`. The logic of
the processing is as follows:

- If the link has a reasonable URI scheme (it starts with `http:`, `https:`,
  `ftp:` or `mailto:`, it is considered "absolute" and taken as is.
- If the link is an absolute path (starts with a `/`), it is considered to be a
  link to a known local page "mount", and it will be changed to precisely point
  to a page at that mount.
- If none of the above applies, the link is considered relative. The path is
  used to find a file relatively from the dirname of the source markdown file.
  The file at that path is then stored in mount
  `/files/xxx/yyyyyyyyyyyyy/<original-filename>` (where `xxx` and `yyyy...` are
  formed by splitting the 16-character SHA256 hash of the file) and linked
  appropriately. This provides a way to store files without the dependency on
  the original directory structure, at the same time the hash acts as a cache
  buster, and (potentially) a content verification helper.
